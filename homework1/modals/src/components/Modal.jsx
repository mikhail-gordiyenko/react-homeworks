import React from 'react'
import PropTypes from 'prop-types'

export default class Modal extends React.Component {
  render() {
    return (
      <div className="modal-cover" onClick={this.props.onClose}>
        <div className="modal" onClick={(e) => e.stopPropagation()}>
          <div className="header">
            <span>{this.props.header}</span>
            {this.props.closeButton && 
              <span className="close-btn" onClick={this.props.onClose}>X</span>
            }
          </div>
          <div className="body">
            <span className="text">{this.props.text}</span>
            {this.props.actions.length > 0 &&
              <div className="actions">
                {this.props.actions.map(action => action)}
              </div>
            }
          </div>
        </div>
      </div>
    )
  }
}

Modal.propTypes = {
  header: PropTypes.string.isRequired,
  text: PropTypes.string.isRequired,
  closeButton: PropTypes.bool.isRequired,
  actions: PropTypes.array
}