import React from 'react'
import PropTypes from 'prop-types'

export default class Button extends React.Component {
  render() {
    return (
      <button className="button" style={{ backgroundColor: this.props.backgroundColor}} onClick={() => this.props.onClick()}>{this.props.text}</button>
    );
  }
}

Button.propTypes = {
  text: PropTypes.string.isRequired,
  backgroundColor: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired
}