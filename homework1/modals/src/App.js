import React from 'react';
import Button from './components/Button'
import Modal from './components/Modal'
import './App.scss';

class App extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      showModal: false
    }
  }

  closeModal() {
    this.setState({ showModal: false })
  }

  render() {
    const okButton = <Button
      backgroundColor="#b3382c"
      text="Ok"
      onClick={() => this.closeModal()}
      key="ok"
    />

    const cancelButton = <Button
      backgroundColor="#b3382c"
      text="Cancel"
      onClick={() => this.closeModal()}
      key="cancel"
    />

    const reactButton = <Button
      backgroundColor="#b3382c"
      text="React"
      onClick={() => this.closeModal()}
      key="react"
    />

    const svelteButton = <Button
      backgroundColor="#b3382c"
      text="Svelte"
      onClick={() => this.closeModal()}
      key="svelte"
    />

    return (
      <div className="App">
        <header className="App-header">
          <h1>My first react App</h1>
        </header>
        <Button
            backgroundColor="#ff0000"
            text="Open first modal"
            onClick={() => {this.setState({showModal: "first"})}}
          />

          <Button
            backgroundColor="#ff00ff"
            text="Open second modal"
            onClick={() => {this.setState({showModal: "second"})}}
          />

          {this.state.showModal && this.state.showModal === "first" && <Modal 
            header="Do you want to delete this file?"
            text="Once you delete this file it wont't be possible to undo this action. Are you sure want to delete it?"
            closeButton={true}
            actions={[okButton, cancelButton]}
            onClose={() => this.closeModal()}
          />}

          {this.state.showModal && this.state.showModal === "second" && <Modal 
            header="What do you want to learn?"
            text="There are many different frameworks and libraries. Which one do you want to learn?"
            closeButton={true}
            actions={[reactButton, svelteButton]}
            onClose={() => this.closeModal()}
          />}
      </div>
    )
  }
}

export default App;
